Name:    jd-gui
Version: 1.6.6
Release: 1%{?dist}
Summary: A standalone Java Decompiler GUI

License: GPL3
URL:     https://java-decompiler.github.io/
Source0: https://github.com/java-decompiler/jd-gui/releases/download/v%{version}/%{name}-%{version}.jar
Source1: https://raw.githubusercontent.com/java-decompiler/jd-gui/v%{version}/src/linux/resources/jd_icon_128.png
Source2: jd-gui
Source3: jd-gui.desktop

BuildArch: noarch

BuildRequires: desktop-file-utils
Requires: java

%description
JD-GUI, a standalone graphical utility that displays Java sources from CLASS files.

%prep

%build

%install
install -Dm644 %{SOURCE0} "%{buildroot}%{_datadir}/java/%{name}/%{name}.jar"
install -d "%{buildroot}%{_bindir}"
install -Dm644 %{SOURCE1} "%{buildroot}%{_datadir}/pixmaps/%{name}.png"
install -Dm755 %{SOURCE2} "%{buildroot}%{_bindir}/%{name}"
desktop-file-install --dir %{buildroot}%{_datadir}/applications %{SOURCE3}

%files
%{_bindir}/%{name}
%{_datadir}/applications/%{name}.desktop
%{_datadir}/java/%{name}/%{name}.jar
%{_datadir}/pixmaps/%{name}.png



%changelog
* Sat Feb 19 2022 Alberto Pedron <albertop2197@gmail.com> - 1.6.6-1
- Initial release
